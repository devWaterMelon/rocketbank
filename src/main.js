import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

Vue.prototype.$store = Vue.observable({
  proxy: null,
  original: null,
  incrLink() {
    const links = this.original.links
    Vue.set(this.original, 'links', links ? links + 1 : 1)
  }
})

new Vue({
  render: h => h(App),
}).$mount('#app')
