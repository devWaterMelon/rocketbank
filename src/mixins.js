export const KeydownMixins = {
  methods: {
    keydown(e) {
      const re = new RegExp('[\\d\\.+\\-\\*\\/=]|Backspace')

      if (!re.exec(e.key)) {
        e.preventDefault()
        return e.stopPropagation()
      }
    }
  }
}
