export const isOperation = (str) => ['+', '-', '*', '/'].includes(str)

export const isEqualOperation = (str) => str === '='

export const isItNumber = (str) => !isNaN(parseFloat(str))
